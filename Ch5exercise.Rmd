---
title: "Ch 5 Exercises"
output: 
  html_document:
    theme: cerulean
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
#options(scipen=999)
library(ggplot2)
library(reshape2)
library(ISLR)
library(boot)
```

## 5.1

Hrmmm...proofs.

## 5.2

> We will now derive the probability that a given observation is part of a bootstrap sample. Suppose that we obtain a bootstrap sample from a set of n observations.

> a. What is the probability that the first bootstrap observation is *not* the jth observation from the original sample? Justify your answer.

Think of this like drawing from a deck of cards: What is the probability that when you draw a card it is *not* the Ace of Spades. n here is 52. Taking out the Ace, we have 51 other cards, so P(not Ace of Spades) = 51/52.

Similarly, given that there are n samples in the data, and we are looking for the probability that we do not pick one of them, we get $P = \frac{n-1}{n}$.

> b. What is the probability that the second bootstrap observation is *not* the jth observation from the original sample? 

This should be the same as before. We're only asking about the second observation, not 1st and second. $P = \frac{n-1}{n}$

> c. Argue that the probability that the jth observation is not in the bootstrap sample is $(1-1/n)^{n}$.

Starting with what we just had - if not in the 1st observation, $\frac{n-1}{n}$. If not in the 2nd observation, $\frac{n-1}{n}$. If not in 1st and not in 2nd, $\frac{n-1}{n}*\frac{n-1}{n} = (\frac{n-1}{n})^{2}$. Continuing this out to not in 1st, 2nd, or 3rd observation, we get $(\frac{n-1}{n})^{3}$. So, for the jth observation to not be in the bootstrap sample, we get $(\frac{n-1}{n})^{n}$, which simplified, yields $(1-1/n)^{n}$.

> d. When n = 5, what is the probability that the jth observation is in the bootstrap sample. 

B/c I really don't want to calculate all possible combinations- in one observation, not the other four (5 of these); in two observations , not the other three (10 of these) etc - let's calculate the probability that the jth observation is not sampled at all. Its complement (1 - p) will give us what we're looking for.

$P(j not in sample) = (\frac{4}{5})^5$ = `r (4/5)^5`

$P(j in sample) = 1 - \frac{4}{5}^5$ = `r 1 - (4/5)^5`

> e. When n = 100, what is the probability that the jth observation is in the bootstrap sample.

$P(j not in sample) = (\frac{99}{100})^{100}$ = `r (99/100)^100`

$P(j in sample) = 1 - (\frac{99}{100})^{100}$ = `r 1 - (99/100)^100`

> f. When n = 10,000, what is the probability that the jth observation is in the bootstrap sample.

$P(j not in sample) = (\frac{9999}{10000})^{10000}$ = `r (9999/10000)^10000`

$P(j in sample) = 1 - (\frac{9999}{10000})^{10000}$ = `r 1 - (9999/10000)^10000`

> g. Create a plot that displays, for each integer value of n from 1 to 100,000 the probability that the jth observation is in the bootstrap sample. Comment on what you observe.

Here I'm going to look at both the probabilities of being in and not being in the sample. These are the complement of each other, so as one decreases, the other increases by the same amount. Of particular interest, is when we look at the probability of the jth observation not being in the sample. As n approaches infinity, this plateaus to 1/e (the horizontal black line).

```{r, echo=FALSE}
n <- 1:100000
in_obs <- 1 - ((n-1)/n)^n
not_in_obs <- ((n-1)/n)^n
df <- data.frame(n,in_obs,not_in_obs)
melt_df <- melt(df, id.vars='n')
ggplot(melt_df, aes(x=n,y=value,color=variable)) + geom_line() + scale_x_log10() +
  scale_y_continuous(limits=c(0,1)) + geom_hline(yintercept=(1/exp(1)))
```

> h. We will now investigate numerically the probability that a bootstrap sample of size n = 100 contains the jth observation. Here, j=4. We repeatedly create bootstrap samples, and each time we record whether or not the fourth observation is contained in the bootstrap sample. Comment on the results obtained

```{r}
store <- rep(NA, 10000)
for(i in 1:10000){
  store[i] <- sum(sample(1:100, rep=TRUE)==4) > 0 # this gives true/false
}
mean(store) #true=1,false=0, mean between 0 and 1
```

HeeeeyyyyY!!!! We get something close to 1 - 1/exp(1)! Excitement!

## 5.3

> k-fold cross-validation

> a. Explain how k-fold cross-validation is implemented

Let's just use k=4 for an example.

1. Randomly divide the dataset into 4 folds  
2. Keeping the 1st fold as the validation set, run model on folds 2-4  
    * Calculate the MSE for fold 1  
3. Repeat step 2 three more times, using folds 2,3, and 4 as the validation set.  
4. Sum the four MSEs together, and divide by 4.  

k-fold CV estimate: $CV_{k} = \frac{1}{k}\sum_{i=1}^{k}{MSE_{i}}$

> b. What are the advantages and disadvantages of k-fold cross validation relative to:

> i. The validation set approach?

The test error rate will be lower with k-fold. Validation Set tends to overestimate the test error rate for the model fit on the entire data set.

> ii. LOOCV?

Computationally less intensive/expensive than LOOCV. LOOCV has higher variance, lower bias.

## 5.4

> Suppose that we use some statistical learning method to make a prediction for the response Y for a particular value of the predictor X. Carefully describe how we might estimate the standard deviation of our prediction. 

Using the bootstrap method, sample the data a number of times (all samples being of the data size n, sampled with replacement). For each sample, run a model on the sample, and predict for the specific value of X. Calculate the stdev of these predictions.

For example:

```{r}
set.seed(42)
x <- 1:10
a = rnorm(10)
y = 2*x + 6 - a
asdf <- rep(0,100) # yes, that's how i name variables when tired
for(i in 1:100) {
  bs.index <- sample(10,10,replace=TRUE) # bs as in bootstrap...
  bs.lm <- lm(y ~ x, subset=bs.index)
  asdf[i] <- predict(bs.lm,data.frame(x=3.5))
}
plot(asdf)
sd(asdf)
```


## 5.5

> In Chapter 4, we used logistic regression to predict the probability of default ising income and balance on the Default data set. We will now estimate the test error of this logistic regression model using the validation set approach. Do not forget to set a random seed before beginning your analysis.

> a. Fit a logistic regression model that uses income and balance to predict default.

```{r}
data('Default')
glm.fit <- glm(default ~ income + balance, data=Default, family=binomial)
glm.fit
```

> b. Using the validation set approach, estimate the test error for this model, using the following steps:  
>
> > i. Split the sample set into a training and validation set.  
> > ii. Fit a multiple logistic regression model using only the training observations.  
> > iii. Obtain a prediction of default status for each individual in the validation set by computing the posterior probability of default for that individual, and classifying the individual to the default category if the posterior probability is > 0.5.  
> > iv. Compute the validation set error, which is the fraction of the observations in the validation set that are misclassified.

```{r}
# writing a function to do this, since the process is repeated below

val.fun <- function(data=Default,seed){
  a <- dim(data)[1] # let's get how many records there are for default
  set.seed(seed) # for reproducibility
  train <- sample(a,(a/2)) # sample 50% of the data
  glm.b <- glm(default ~ income + balance, data=data, family=binomial,
               subset=train) # train the model
  # print(summary(glm.b)) # uncomment if you want to see the model output
  b.probs <- predict(glm.b,data,type='response')[-train] # get the predicted probabilities
  b.pred <- rep('No',5000) # setting up the predicted response
  b.pred[b.probs > .5] <- 'Yes' # changing those responses that should be yes
  results <- table(b.pred,data[-train,'default']) # table of predicted and true
  print(results) # so we can see things
  seterror <- (results[2,1]+results[1,2])/sum(results) # percent incorrect
  seterror
}

val.fun(Default,42)
```

> c. Repeat the process in (b) 3 times, using three different splits of the observations into a training and validation set.

```{r}
val.fun(Default,8)
val.fun(Default,34)
val.fun(Default,421)
```

Overall, similar results, but man, this model is mediocre at determining who is actually going to default. For each of the different splits, both variables are considered significant.

> d. Now consider a logistic regression model that predicts the probability of default using income, balance, and a dummy variable for student. Estimate the test error for this model using the validation set approach. Comment on whether or not including a dummy variable for student leads to a reduction in the test error rate.


```{r}
a <- dim(Default)[1] # let's get how many records there are for default
set.seed(42) # for reproducibility
train <- sample(a,(a/2))
glm.d <- glm(default ~ income + balance + student, data=Default, family=binomial,
               subset=train)
summary(glm.d)
d.probs <- predict(glm.d,Default,type='response')[-train]
d.pred <- rep('No',5000)
d.pred[d.probs > .5] <- 'Yes'
results <- table(d.pred,Default[-train,'default']) # table of predicted and true
results
seterror <- (results[2,1]+results[1,2])/sum(results)
seterror
```

The error is about the same, so test error is not improved by adding in the dummy variable. Really, the only difference seems to be that the coefficients now indicate that student is a significant variable, while income is not.

## 5.6

> Continue to use logistic regression to predict the probability of default using income and balance on the Default data set. Compute estimates for the standard errors of the income and balance coefficients in two different ways: 1) using the bootstrap, 2) using the standard formula for computing the standard errors in the `glm` function. 

> a. Using the `summary` and `glm` functions, determine the estimated standard errors for the cofficients associated with income and balance in a multiple logistic regression models that uses both predictors.

```{r}
fit.a <- glm(default ~ income + balance, data=Default, family=binomial)
summary(fit.a)
```

The estimated standard error of income is `r summary(fit.a)$coefficients['income','Std. Error']`, and the estimated standard error of balance is `r summary(fit.a)$coefficients['balance','Std. Error']`

> b. Write a function, `boot.fn` that takes as input the Default data set as well as an index of the observations, and that outputs the coefficient estimates for income and balance in the model.

Using the same idea as the function written in the lab...

```{r}
boot.fn <- function(data=Default, index){
  fit <- glm(default ~ income + balance, data=data, family=binomial, subset=index)
  return(coef(fit))
}

boot.fn(Default,1:dim(Default)[1]) # making sure it does what i think it does
```

> c. Use the `boot` function together with your `boot.fn` function to estimate the standard errors of the coefficients for income and balance.

```{r}
set.seed(456)
boot.res <- boot(Default, boot.fn,100)
boot.res
```

> d. Comment on the estimated standard errors obtained using the `glm` function and using your bootstrap function.

The bootstrap results in pretty similar errors to that using the entire data set in the glm function.
